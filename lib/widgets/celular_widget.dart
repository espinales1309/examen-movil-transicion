import 'package:flutter/material.dart';
import '../constants.dart';
import '../model/email.dart';
import '../model/zapato.dart';

class EmailWidget extends StatelessWidget {
  final Zapato zapato;
  final Function onTap;


  const EmailWidget(
      {Key? key,
      required this.zapato,
      required this.onTap,
      })
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onHorizontalDragEnd: (DragEndDetails details) {
      },
      onLongPress: () {
      },
      onTap: () {
        onTap(zapato);
      },
      
      child: Container(
      
        padding: const EdgeInsets.all(10.0),
        height: 80.0,

        child: Row(
       
          
          children: <Widget> [
            
            
            Expanded(
              flex: 1,
              child: Container(
                height: 12.0,
                
              ),
              
            ),
            Expanded(
              flex: 9,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: [
              
                  Text('PRECIO: ${zapato.precio}',
                      style: dateTextStyle),
                  Text('NOMBRE: ${zapato.nombre}', style: fromTextStyle),
                  Text('MODELO:${zapato.modelo}', style: subjectTextStyle),
                ],
              ),
            )
       
          ],
        ),
      ),
    );
  }
}
