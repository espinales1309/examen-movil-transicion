import 'package:flutter/cupertino.dart';

class Zapato {
  final int id;
  final String nombre;
  final String talla;
  final String precio;
  final String modelo;

  Zapato({
    required this.id,
    required this.nombre,
    required this.talla,
    required this.precio,
    required this.modelo,
  });
}

